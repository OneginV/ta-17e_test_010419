const user = require("../src/user");

it("snapshot testing, ID 1", () => {
 const u = user(1);
 expect(u).toMatchSnapshot();
});
it("snapshot testing, ID 56", () => {
 expect(user(56)).toMatchSnapshot();
});
it("snapshot testing, ID 1345", () => {
 expect(user(1345)).toMatchSnapshot();
});
it("snapshot testing, passing a string as ID", () => {
 expect(() => {
  user("a");
 }).toThrow();

});